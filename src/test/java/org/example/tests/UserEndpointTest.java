package org.example.tests;

import io.restassured.response.Response;
import org.example.helper.RandomUserCreator;
import org.example.model.CreateUserModel;
import org.example.services.RestClient;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public class UserEndpointTest {

    @Test
    public void testAllUserIdValidity() {
        Response response = RestClient.getUsers();
        Assert.assertEquals(response.getStatusCode(), 200);
        List<Map<String, ?>> users = response.jsonPath().getList("data");

        for (Map<String, ?> user : users) {
            int userId = (int) user.get("id");
            //user id's are greater than 4 digits so I controlled if user id is 4 digit
            Assert.assertFalse(userId >= 1000 && userId <= 9999, "User ID is a 4-digit integer");
            Assert.assertNotNull(user.get("id"), "User ID is null");
        }


    }

    @Test
    public void testCreateUserAndMatch() throws IOException {

        RandomUserCreator.CreateUserResult result = RandomUserCreator.randomUserCreator();
        String name = result.getName();
        CreateUserModel createUserModel = result.getCreateUserModel();


        Response createUserResponse = RestClient.createUser(createUserModel);

        Assert.assertEquals(createUserResponse.getStatusCode(), 201);

        String actualName = createUserResponse.jsonPath().getString("data.name");
        String actualEmail = createUserResponse.jsonPath().getString("data.email");
        String actualGender = createUserResponse.jsonPath().getString("data.gender");
        String actualStatus = createUserResponse.jsonPath().getString("data.status");

        Assert.assertEquals(actualName, name, "Name mismatch");
        Assert.assertEquals(actualEmail, name + "@example.com", "Email mismatch");
        Assert.assertEquals(actualGender, "male", "Gender mismatch");
        Assert.assertEquals(actualStatus, "active", "Status mismatch");
    }


    @Test
    public void testCreateSameUserToTakeError() throws IOException {

        String lastResponse = null;
        RandomUserCreator.CreateUserResult result = RandomUserCreator.randomUserCreator();
        CreateUserModel createUserModel = result.getCreateUserModel();
        String name = result.getName();


        for (int i = 0; i < 2; i++) {
            Response createUserResponse = RestClient.createUser(createUserModel);
            lastResponse = createUserResponse.getBody().jsonPath().getString("data[0].message");
        }
        System.out.println("Response Message: " + lastResponse);
        Assert.assertEquals(lastResponse, "has already been taken");


    }


}


