package org.example.helper;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.example.model.CreateUserModel;

import java.io.File;
import java.io.IOException;

public class JsonBodyModelHelper {

    public static CreateUserModel createUserModel() throws IOException {

        ObjectMapper mapper = new ObjectMapper();

        CreateUserModel createUserModel = mapper.readValue(new File("src/main/resources/jsonFiles/postV1Users.json"), CreateUserModel.class);
        return createUserModel;

    }
}