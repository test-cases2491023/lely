package org.example.helper;

import org.apache.commons.lang3.RandomStringUtils;

public class RandomHelper {

    public static String generateRandomWord(int length) {
        return RandomStringUtils.random(length, true, true);
    }
}
