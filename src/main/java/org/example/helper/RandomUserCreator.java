package org.example.helper;

import org.example.model.CreateUserModel;

import java.io.IOException;

public class RandomUserCreator {
    public static class CreateUserResult {
        private final CreateUserModel createUserModel;
        private final String name;

        public CreateUserResult(CreateUserModel createUserModel, String name) {
            this.createUserModel = createUserModel;
            this.name = name;
        }

        public CreateUserModel getCreateUserModel() {
            return createUserModel;
        }

        public String getName() {
            return name;
        }
    }

    public static CreateUserResult randomUserCreator() throws IOException {


        String name = RandomHelper.generateRandomWord(7);
        CreateUserModel createUserModel = JsonBodyModelHelper.createUserModel();
        createUserModel.setEmail(name + "@example.com");
        createUserModel.setName(name);

        return new CreateUserResult(createUserModel, name);
    }
}
