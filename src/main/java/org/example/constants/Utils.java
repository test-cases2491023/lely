package org.example.constants;

import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class Utils {
    public static String ACCESS_TOKEN;

    static {
        try {
            Properties properties = new Properties();
            properties.load(new FileReader("src/config.properties"));
            ACCESS_TOKEN = properties.getProperty("access_token");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}