package org.example.services;

import io.restassured.response.Response;
import org.example.constants.Url;
import org.example.constants.Utils;
import org.example.model.CreateUserModel;

import static io.restassured.RestAssured.given;

public class RestClient {

    public static Response getUsers() {
        return given()
                .header("Authorization", "Bearer " + Utils.ACCESS_TOKEN)
                .when()
                .get(Url.BASE_URL + "/public/v1/users")
                .then()
                .extract()
                .response();

    }

    public static Response createUser(CreateUserModel createUserModel) {
        return given()
                .header("Authorization", "Bearer " + Utils.ACCESS_TOKEN)
                .contentType("application/json")
                .when()
                .body(createUserModel)
                .post(Url.BASE_URL + "/public/v1/users")
                .then()
                .extract()
                .response();

    }

}
